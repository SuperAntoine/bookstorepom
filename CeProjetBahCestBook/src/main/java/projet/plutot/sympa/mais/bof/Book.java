package projet.plutot.sympa.mais.bof;

import net.tncy.validator.constraints.books.ISBN;

public class Book {
    private String name;
    private String author;
    @ISBN
    private String isbn;

    public Book(String name, String author, String isbn) {
        this.name = name;
        this.author = author;
        this.isbn = isbn;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn() {
        return isbn;
    }
}
