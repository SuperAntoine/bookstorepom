package projet.plutot.sympa.mais.bof;

import java.util.ArrayList;

public class Bookstore {
    private ArrayList<Book> books = new ArrayList<>();

    public void addBook(Book book) {
        this.books.add(book);
    }

    public void removeBook(Book book) {
        this.books.remove(book);
    }

    public void updateBook(String bookId, Book book) {}

    public Book getBookById(String bookId) {
        return null;
    }

    public Book[] findBookByTitle(String Title) {
        return null;
    }
    
}